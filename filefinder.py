#! /usr/bin/python
# -*- coding: utf-8 -*-

# Simple Python script that will execute system command(s) in a complete directorie tree.
# In this release, the commande si a ffmpeg command line to convert flac files to mp3 files
# Change the content of cmd variable to use any other command

import os
import sys
import argparse

parser = argparse.ArgumentParser(description='Directories runner')
parser.add_argument('-d','--directory', help='Directory to scan (default: %(default)s)', default=".")
args = parser.parse_args()

walk_dir = args.directory

print('walk_dir = ' + walk_dir)

# If your current working directory may change during script execution, it's recommended to
# immediately convert program arguments to an absolute path. Then the variable root below will
# be an absolute path as well. Example:
# walk_dir = os.path.abspath(walk_dir)
print('walk_dir (absolute) = ' + os.path.abspath(walk_dir))

for root, subdirs, files in os.walk(walk_dir):
    for filename in files:
        file_path = os.path.join(root, filename)
        if filename.endswith(".flac"):
            cmd = "ffmpeg -y -i \"" + file_path + "\" -vn -acodec libmp3lame -b:a 192k \"" + file_path.replace("flac", "mp3") + "\""
            os.system(cmd)
            
            # uncomment this line if you are sure to want to delete the targeted file
            # os.remove(file_path)
